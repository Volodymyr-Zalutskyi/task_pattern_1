import style.pizza.KyivStyleCheesePizza;
import style.pizza.LvivStyleCheesePizza;
import style.pizza.Pizza;

public class CountryPizza extends PizzaStore {
   public Pizza createPizza(String item, String coutry) {
       if(item.equals("cheese") && coutry.equals("Kyiv")) {
           return new KyivStyleCheesePizza();
       }else if(item.equals("cheese") && coutry.equals("Lviv")) {
           return new LvivStyleCheesePizza();
       }else{
           return null;
       }
   }
}
