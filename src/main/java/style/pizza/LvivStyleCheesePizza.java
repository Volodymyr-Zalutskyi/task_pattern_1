package style.pizza;

public class LvivStyleCheesePizza extends Pizza {

    public LvivStyleCheesePizza() {
        name = "Lviv Style Deep Dish Cheese style.pizza.Pizza";
        dough = "Extra Thin Crush Dough";
        sauce = "Plum Tomato Sauce";

        toppings.add("Shredded Mozzarella Cheese");
    }

    public void cut() {
        System.out.println("Cutting pizza into square slices");
    }

}
