package style.pizza;

public class KyivStyleCheesePizza extends Pizza {

    public KyivStyleCheesePizza() {
        name = "Kyiv Style and Cheese style.pizza.Pizza";
        dough = "Thin Crush Dough";
        sauce = "Marinara sauce";
        toppings.add("Grated Reggiano Cheese");
    }
}
