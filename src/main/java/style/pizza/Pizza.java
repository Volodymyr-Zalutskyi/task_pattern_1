package style.pizza;

import java.util.ArrayList;

public abstract class Pizza {
    public String name;
    public String dough;
    public String sauce;
    public ArrayList<String> toppings = new ArrayList<String>();

    public void prepare() {
        System.out.println("Prepare " + name);
        System.out.println("Tossing dough ...");
        System.out.println("Adding sauce ...");
        System.out.println("Adding topping :");
        for(String topping : toppings) {
            System.out.println(" " + topping);
        }
    }

    public void bake () {
        System.out.println("Bake for 25 minutes at 350");
    }

    public void cut () {
        System.out.println("Cut the pizza into diagonal slice");
    }

    public void box () {
        System.out.println("Place pizza in official PizzaStore box");
    }

    public String getName() {
        return name;
    }

}
