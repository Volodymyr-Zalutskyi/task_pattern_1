import style.pizza.Pizza;

public abstract class PizzaStore {

    abstract Pizza createPizza (String itme, String country);

    public Pizza orderPizza(String type, String country) {
        Pizza pizza = createPizza(type, country);
        System.out.println("Making a " + pizza.getName());
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
